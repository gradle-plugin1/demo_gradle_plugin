plugins {
    `java-gradle-plugin`
    `kotlin-dsl`
    `maven-publish`
    // Apply the Plugin Publish Plugin to make plugin publication possible
    id("com.gradle.plugin-publish") version "0.20.0"
    kotlin("jvm") version "1.5.10"
}

group = "com.nathan.plugin.demo"
version = "0.1.3"

repositories {
    mavenCentral()
    google()
}

pluginBundle {
    website = "https://gitlab.com/gradle-plugin1/demo_gradle_plugin"
    vcsUrl = "https://gitlab.com/gradle-plugin1/demo_gradle_plugin"
    description = "Demo print hello world plugin"
    tags = listOf("demo")
}

gradlePlugin {
    plugins {
        create("nathanDemoPlugin") {
            id = group.toString()
            displayName = "Demo plugin"
            implementationClass = "com.nathan.plugin.demo.HelloWorldPlugin"
        }
    }
}

publishing {
    publications {
    }
    repositories {
        maven {
            setUrl("$projectDir/repository/")
        }
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("com.android.tools.build:gradle:4.0.2")
}

tasks.withType<Copy> {
    duplicatesStrategy = DuplicatesStrategy.INCLUDE
}