package com.nathan.plugin.demo

import org.gradle.api.Plugin
import org.gradle.api.Project

class HelloWorldPlugin: Plugin<Project> {
    override fun apply(target: Project) {
        print("\n ${target.name}: HelloWorldPlugin: 0.1.2 Hello there!!\n")
    }
}